<?php

namespace Lamotivo\Assets;

class RawCss extends RawAsset
{
    /**
     * CSS rules.
     *
     * @var array
     */
    protected $rules = [];

    /**
     * Featur: merge selectors.
     *
     * @var boolean
     */
    protected $mergeSelectors = false;

    /**
     * @return string
     */
    public function hash()
    {
        // Make the content frozen once hash() is called
        if ($this->rules) {
            $content = $this->content();
            $this->rules = [];
            $this->content = $content;
        }
        return 'css-' . md5($this->content());
    }

    /**
     * @param boolean
     */
    public function mergeSelectors($value = true)
    {
        $this->mergeSelectors = (bool)$value;
    }

    /**
     * @return $this
     */
    public function reset()
    {
        $this->rules = [];
        return parent::reset();
    }

    /**
     * Add a new CSS rule with given properties.
     *
     * @param  string  $selector
     * @param  string|array  $properties
     * @return $this
     */
    public function addRule($selector, $properties)
    {
        if (! isset($this->rules[$selector])) {
            $this->rules[$selector] = [];
        }
        if (is_string($properties)) {
            $properties = explode(';', $properties);
        }
        foreach ($properties as $key => $value) {
            if (! is_string($key)) {
                list($key, $value) = explode(':', trim($value));
            }
            $this->rules[$selector][$key] = trim($value);
        }
        return $this;
    }

    /**
     * Add custom CSS properties (variables) to a selector.
     *
     * @param  string  $selector
     * @param  string|array  $variables
     * @return $this
     */
    public function addVariables($selector, $variables)
    {
        if (! isset($this->rules[$selector])) {
            $this->rules[$selector] = [];
        }
        if (is_string($variables)) {
            $variables = explode(';', $variables);
        }
        foreach ($variables as $key => $value) {
            if (! is_string($key)) {
                list($key, $value) = explode(':', trim($value));
            }
            $this->rules[$selector]['--' . trim($key, '-')] = trim($value);
        }
        return $this;
    }

    /**
     * Get the CSS rule with given selector.
     *
     * @param  string  $selector
     * @return array
     */
    public function getRule($selector)
    {
        if (isset($this->rules[$selector])) {
            return $this->rules[$selector];
        }
        return [];
    }

    /**
     * @return string
     */
    public function content()
    {
        $content = (string)$this->content;

        if ($this->rules) {
            $rules = [];

            foreach ($this->rules as $selector => $properties) {
                $rules[$selector] = implode(';', $this->processProperties($properties));
            }

            if ($this->mergeSelectors) {
                $rules = $this->mergeRules($rules);
            }

            foreach ($rules as $selector => $properties) {
                $content .= $selector . '{' . $properties . '}';
            }
        }

        return $content;
    }

    /**
     * @param array
     * @return array
     */
    protected function processProperties($properties)
    {
        $result = [];

        foreach ($properties as $key => $value) {
            $result[] = $key . ':' . $value;
        }

        return $result;
    }

    /**
     * @param array
     * @return array
     */
    protected function mergeRules($rules)
    {
        $inverse = [];
        foreach ($rules as $selector => $properties) {
            if (! isset($inverse[$properties])) {
                $inverse[$properties] = [];
            }
            $inverse[$properties][] = $selector;
        }

        $rules = [];
        foreach ($inverse as $properties => $selectors) {
            $rules[implode(',', $selectors)] = $properties;
        }

        return $rules;
    }
}
