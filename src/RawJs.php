<?php

namespace Lamotivo\Assets;

class RawJs extends RawAsset
{
    /**
     * @return string
     */
    public function hash()
    {
        return 'js-' . md5($this->content);
    }
}
