<?php

namespace Lamotivo\Assets;

class RawScss extends RawCss
{
    /**
     * @return string
     */
    public function hash()
    {
        return 'scss-' . md5($this->content());
    }
}
