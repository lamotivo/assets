<?php

namespace Lamotivo\Assets\Filters;

use Lamotivo\Assets\AssetManager;
use Lamotivo\Assets\RawAsset;
use tubalmartin\CssMin\Minifier;

class CssRewriteUrls extends Filter
{
    /**
     * Rewrite relative URLs in CSS files to take account of their new location
     *
     * @param string $data                  The data to be filtered
     * @param string|RawAsset $asset_url    The original asset URL or RawAsset object
     * @param AssetManager $asset           The asset manager
     *
     * @return string
     */
    public function filter($data, $asset_url, $asset)
    {
        if (is_string($asset_url) && $this->isAbsoluteUrl($asset_url))
        {
            $prefix = dirname($asset_url);

            $data = preg_replace_callback([
                '/(\burl\s*\(\s*")([^"]+?)("\s*\))/',
                '/(\burl\s*\(\s*\')([^\']+?)(\'\s*\))/',
                '/(\burl\s*\(\s*)([^\'"]+?)(\s*\))/',
            ], function ($matches) use ($prefix) {
                if ($this->isAbsoluteUrl($matches[2]))
                {
                    return $matches[0];
                }
                else
                {
                    return $matches[1] . $this->normalizePath($prefix . '/' . $matches[2]) . $matches[3];
                }
            }, $data);
        }

        return $data;
    }

    /**
     * Is a URL absolute
     *
     * @param string $url
     *
     * @return bool
     */
    protected function isAbsoluteUrl($url)
    {
        return preg_match('/^((https?:)?\/\/|data:)/i', $url);
    }

    /**
     * Normalize a path
     *
     * @param string $url
     *
     * @return string
     */
    protected function normalizePath($url)
    {
        while (strpos($url, '/./') !== false) {
            $url = str_replace('/./', '/', $url);
        }
        while (strpos($url, '/../') !== false) {
            $url = preg_replace('/[^\/]+\/\.\.\//', '', $url, 1);
        }
        return $url;
    }

}
