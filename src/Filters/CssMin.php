<?php

namespace Lamotivo\Assets\Filters;

use Illuminate\Support\Str;
use tubalmartin\CssMin\Minifier;
use Lamotivo\Assets\AssetManager;
use Lamotivo\Assets\RawAsset;

class CssMin extends Filter
{
    /**
     * Minify CSS files using tubalmartin/cssmin
     *
     * @param string $data                  The data to be filtered
     * @param string|RawAsset $asset_url    The original asset URL or RawAsset object
     * @param AssetManager $asset           The asset manager
     *
     * @return string
     */
    public function filter($data, $asset_url, $asset)
    {
        $shouldBeFiltered = true;

        if (is_string($asset_url)) {
            $shouldBeFiltered = !Str::endsWith($asset_url, ['.min.css', '-min.css']);
        }

        if ($shouldBeFiltered) {
            $data = (new Minifier())->run($data);
        }

        return $data;
    }
}
