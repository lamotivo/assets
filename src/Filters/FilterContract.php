<?php

namespace Lamotivo\Assets\Filters;

use Lamotivo\Assets\AssetManager;
use Lamotivo\Assets\RawAsset;

interface FilterContract
{
    /**
     * @param string $data                  The data to be filtered
     * @param string|RawAsset $asset_url    The original asset URL or RawAsset object
     * @param AssetManager $asset           The asset manager
     *
     * @return string
     */
    public function filter($data, $asset_url, $asset);

    /**
     * Allow the object to be serialized in laravel's config cache
     *
     * @return static
     */
    public static function __set_state(array $array = null);
}
