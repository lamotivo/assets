<?php

namespace Lamotivo\Assets\Filters;

use Lamotivo\Assets\AssetManager;

abstract class Filter implements FilterContract
{
    /**
     * @param string $data                  The data to be filtered
     * @param string|RawAsset $asset_url    The original asset URL or RawAsset object
     * @param AssetManager $asset           The asset manager
     *
     * @return string
     */
    public function filter($data, $asset_url, $asset)
    {
        return $data;
    }

    /**
     * Allow serialization for Laravel config cache
     *
     * @return static
     */
    public static function __set_state(array $array = null)
    {
        return new static;
    }
}
