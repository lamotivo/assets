<?php

namespace Lamotivo\Assets\Filters;

use Illuminate\Support\Str;
use ScssPhp\ScssPhp\Compiler;
use ScssPhp\ScssPhp\OutputStyle;
use ScssPhp\ScssPhp\Formatter\Expanded as ExpandedFormatter;
use ScssPhp\ScssPhp\Formatter\Nested as NestedFormatter;
use ScssPhp\ScssPhp\Formatter\Compressed as CompressedFormatter;
use ScssPhp\ScssPhp\Formatter\Compact as CompactFormatter;
use ScssPhp\ScssPhp\Formatter\Crunched as CrunchedFormatter;
use Lamotivo\Assets\AssetManager;
use Lamotivo\Assets\RawAsset;
use Lamotivo\Assets\RawScss;


class ScssCompiler extends Filter
{
    /**
     * Compile SCSS files to CSS
     *
     * @param string $data                  The data to be filtered
     * @param string|RawAsset $asset_url    The original asset URL or RawAsset object
     * @param AssetManager $asset           The asset manager
     *
     * @return string
     */
    public function filter($data, $asset_url, $asset)
    {
        $shouldBeFiltered = false;

        if (is_string($asset_url)) {
            $absoluteUrl = preg_match('/^((https?:)?\/\/|data:)/i', $asset_url);
            if ($absoluteUrl) {
                $shouldBeFiltered = false;
            } else {
                if (Str::endsWith($asset_url, '.scss')) {
                    $shouldBeFiltered = true;
                }
            }
        } else {
            $shouldBeFiltered = is_a($asset_url, RawScss::class);
        }

        if ($shouldBeFiltered) {
            $paths = $asset->getScssPaths();
            if (is_string($asset_url)) {
                $paths[] = dirname($asset_url);
            }

            $compiler = new Compiler;
            $compiler->setImportPaths($paths);
            $compiler->addImportPath($asset->getScssImportFunction());
            $compiler->addVariables($asset->getScssVars());
            if (method_exists($compiler, 'setFormatter')) {
                // scssphp < v2.0
                $compiler->setFormatter($this->resolveFormatter($asset->getScssFormatter()));
            } else {
                // scssphp >= v2.0
                $compiler->setOutputStyle($this->resolveOutputStyle($asset->getScssFormatter()));
            }

            foreach ($asset->getScssFunctions() as $name => $callable) {
                $argumentDeclaration = [];
                if (is_array($callable) && count($callable) >= 2) {
                    $argumentDeclaration = $callable[1];
                    $callable = $callable[0];
                } else {
                    if (class_exists(OutputStyle::class) && method_exists($callable, 'getArgumentDeclaration')) {
                        $argumentDeclaration = $callable::getArgumentDeclaration();
                    }
                }
                if (class_exists(OutputStyle::class)) {
                    $compiler->registerFunction($name, $callable, $argumentDeclaration);
                } else {
                    $compiler->registerFunction($name, $callable);
                }
            }

            if (method_exists($compiler, 'compile')) {
                // scssphp < v2.0
                $data = $compiler->compile($data);
            } else {
                // scssphp >= v2.0
                $data = $compiler->compileString($data)->getCss();
            }
        }

        return $data;
    }

    /**
     * Resolve SCSS formatter name
     *
     * This resolves plain name to full qualified class name.
     * It supports resolving names from leafo/scssphp package.
     *
     * @param string $name          The name of a formatter
     *
     * @return string
     */
    protected function resolveFormatter($name)
    {
        switch ($name) {
            case 'expanded':
            case \Leafo\ScssPhp\Formatter\Expanded::class:
                $name = ExpandedFormatter::class;
                break;

            case 'nested':
            case \Leafo\ScssPhp\Formatter\Nested::class:
                $name = NestedFormatter::class;
                break;

            case 'compact':
            case \Leafo\ScssPhp\Formatter\Compact::class:
                $name = CompactFormatter::class;
                break;

            case 'compressed':
            case \Leafo\ScssPhp\Formatter\Compressed::class:
                $name = CompressedFormatter::class;
                break;

            case 'crunched':
            case \Leafo\ScssPhp\Formatter\Crunched::class:
                $name = CrunchedFormatter::class;
                break;
        }

        return $name;
    }

    protected function resolveOutputStyle($name)
    {
        switch ($name) {
            case 'expanded':
            case \Leafo\ScssPhp\Formatter\Expanded::class:
            case 'nested':
            case \Leafo\ScssPhp\Formatter\Nested::class:
                $name = OutputStyle::EXPANDED;
                break;

            case 'compact':
            case \Leafo\ScssPhp\Formatter\Compact::class:
            case 'compressed':
            case \Leafo\ScssPhp\Formatter\Compressed::class:
            case 'crunched':
            case \Leafo\ScssPhp\Formatter\Crunched::class:
                $name = OutputStyle::COMPRESSED;
                break;
        }

        return $name;
    }
}

