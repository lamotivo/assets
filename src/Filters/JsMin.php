<?php

namespace Lamotivo\Assets\Filters;

use Illuminate\Support\Str;
use JShrink\Minifier;
use Lamotivo\Assets\AssetManager;
use Lamotivo\Assets\RawAsset;

class JsMin extends Filter
{
    /**
     * Minify CSS files using tedivm/jshrink
     *
     * @param string $data                  The data to be filtered
     * @param string|RawAsset $asset_url    The original asset URL or RawAsset object
     * @param AssetManager $asset           The asset manager
     *
     * @return string
     */
    public function filter($data, $asset_url, $asset)
    {
        $shouldBeFiltered = true;

        if (is_string($asset_url)) {
            $shouldBeFiltered = !Str::endsWith($asset_url, ['.min.js', '-min.js']);
        }

        if ($shouldBeFiltered) {
            $data = Minifier::minify($data);
        }

        return $data;
    }
}
