<?php

namespace Lamotivo\Assets;

class RawAsset
{
    /**
     * Raw asset content.
     *
     * @var string
     */
    protected $content = '';

    /**
     * Create a new RawAsset instance.
     *
     * @param  string  $content
     * @return void
     */
    public function __construct($content = '')
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->content();
    }

    /**
     * @return string
     */
    public function content()
    {
        return (string)$this->content;
    }

    /**
     * @return string
     */
    public function hash()
    {
        return md5($this->content);
    }

    /**
     * @return $this
     */
    public function reset()
    {
        $this->content = '';
        return $this;
    }
}
