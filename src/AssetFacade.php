<?php

namespace Lamotivo\Assets;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Lamotivo\Assets\AssetManager
 */
class AssetFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'asset.manager';
    }
}
