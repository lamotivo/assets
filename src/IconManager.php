<?php

namespace Lamotivo\Assets;

use InvalidArgumentException;

use Stringable;
use Illuminate\Support\HtmlString;
use Illuminate\Contracts\Support\Htmlable;

class IconManager implements Htmlable, Stringable
{
    /**
     * @var string
     */
    public static $CSS_CLASS = '';

    /**
     * @var string
     */
    public static $CSS_PREFIX = 'svg-icon-';

    /**
     * @var string
     */
    public static $ID_PREFIX = 'svg-shape-';

    /**
     * @var array
     */
    protected static $icons = [];

    /**
     * @var AssetManager
     */
    protected $assets;

    /**
     * @var RawCss
     */
    protected static $css;

    public function __construct(AssetManager $assets)
    {
        $this->assets = $assets;
        if (! isset(static::$css)) {
            static::$css = new RawCss;
            static::$css->mergeSelectors();
        }
    }

    /**
     * Add an icon to the IconMap and pass it to the HTML code.
     *
     * @param string $svgName                  SVG name
     * @param string|array|\Closure $classes   Classes appended to SVG tag
     *
     * @return HtmlString
     *
     * @throws InvalidArgumentException
     */
    public function icon($svgName, $classes = null)
    {
        $svgName = $this->normalizeSvgName($svgName);

        $id = $this->normalizeSvgId($svgName);

        if (! $this->hasIcon($id)) {
            $svgFileName = $this->assets->findAsset($this->normalizeFileName($svgName));

            $svg = file_get_contents($svgFileName);
            if (! $this->pushIcon($id, $svg)) {
                new InvalidArgumentException('Invalid SVG code provided');
            }
        }

        $preClass = static::$CSS_CLASS ? static::$CSS_CLASS . ' ' : '';

        return new HtmlString('<svg class="' . $preClass . static::$CSS_PREFIX . $id . $this->parseClasses($classes) . '"><use xlink:href="#' . static::$ID_PREFIX . $id . '" /></svg>');
    }

    /**
     * Get content as a string of HTML.
     *
     * @return string
     */
    public function __toString()
    {
        return (string)$this->map();
    }

    /**
     * Get content as a string of HTML.
     *
     * @return string
     */
    public function toHtml()
    {
        return (string)$this;
    }

    /**
     * Render SVG map to the HTML code.
     *
     * @return HtmlString
     */
    public function map()
    {
        return new HtmlString('<svg xmlns="http://www.w3.org/2000/svg" style="display: none;" aria-hidden="true">' . implode('', static::$icons) . '</svg>');
    }

    /**
     * Add an icon or icons to the IconMap.
     *
     * @param string|array $icons     Icons
     *
     * @return void
     *
     * @throws InvalidArgumentException
     */
    public function add($icons)
    {
        if (is_string($icons)) {
            return $this->icon($icons);
        }

        if (!is_array($icons)) {
            $icons = [$icons];
        }

        foreach ($icons as $icon) {
            $this->icon($icon);
        }
    }

    /**
     * Check whether an SVG icon has been already pushed to IconMap.
     *
     * @param string $id     Icon ID
     *
     * @return boolean
     */
    public function hasIcon($id)
    {
        $id = $this->normalizeSvgId($id);
        return isset(static::$icons[$id]);
    }

    /**
     * Push an SVG icon to the IconMap with given ID.
     *
     * @param string $id        Icon ID
     * @param string $svg       SVG code
     * @param boolean $force    Force to replace an existing icon
     *
     * @return boolean
     */
    public function pushIcon($id, $svg, $force = false)
    {
        $id = $this->normalizeSvgId($id);

        if ($this->hasIcon($id) && ! $force) {
            return true;
        }

        if (preg_match('~<svg(.*?)>(.+)</svg>~is', $svg, $matches)) {
            $attributes = $matches[1];
            $svg = $matches[2];

            // detect inline width
            $width = 0;
            if (preg_match('~ width="(\d+)"~', $attributes, $m)) {
                $width = (int)$m[1];
            }

            // detect inline height
            $height = 0;
            if (preg_match('~ height="(\d+)"~', $attributes, $m)) {
                $height = (int)$m[1];
            }

            // preserve viewBox
            $viewBox = '';
            if (preg_match('~ viewBox="(\-?\d+) (\-?\d+) (\d+) (\d+)"~', $attributes, $m)) {
                $viewBox = $m[0];
                if (! $width || ! $height) {
                    $width = $m[3];
                    $height = $m[4];
                }
            }

            if ($width) {
                $this->setIconSize($id, $width, $height);
            }

            // preserve fill
            $fill = 'currentColor';
            if (preg_match('~ fill="(.+?)"~', $attributes, $m)) {
                $fill = $m[1];
            }

            // preserve stroke
            $strokes = [];
            $stroke = '';
            if (preg_match_all('~ (stroke(?:-\w+)?)="(.+?)"~', $attributes, $m)) {
                foreach ($m[0] as $i => $_) {
                    if ($m[1][$i] === 'stroke') {
                        $stroke = $m[2][$i];
                    }
                    $strokes[] = $_;
                }
            }
            if ($fill === 'none' && !$stroke) {
                $strokes[] = 'stroke="currentColor"';
            }

            $svg = '<symbol id="' . static::$ID_PREFIX . $id . '"' . $viewBox . ' fill="' . $fill . '"' . ($strokes ? ' ' . implode(' ', $strokes) : '') . '>' . $svg . '</symbol>';

            static::$icons[$id] = $svg;

            return true;
        }

        return false;
    }

    /**
     * Set icon size by ID.
     *
     * @param integer|string $width
     * @param integer|string $height
     */
    public function setIconSize($id, $width, $height = 0)
    {
        $id = $this->normalizeSvgId($id);

        if (! $height) {
            $height = $width;
        }

        if (is_numeric($width)) {
            $width = $width . 'px';
        }

        if (is_numeric($height)) {
            $height = $height . 'px';
        }

        static::$css->addRule('.' . static::$CSS_PREFIX . $id, [
            'width' => $width,
            'height' => $height,
        ]);
    }

    /**
     * Set icon sizes by their IDs.
     *
     * @param array $sizes
     * @param string|integer|array $defaultSize
     */
    public function setIconSizes($sizes, $defaultSize = '1em')
    {
        foreach ($sizes as $id => $size) {
            if (is_string($id)) {
                if (!$size) {
                    $size = $defaultSize;
                }
            } else {
                $id = $size;
                $size = $defaultSize;
            }
            $size = (array)$size;
            $this->setIconSize($id, ...$size);
        }
    }

    /**
     * Parse classes from array, CLosure or string.
     *
     * @param string|array|\Closure $classes    Classes appended to SVG tag
     *
     * @return string
     *
     * @throws InvalidArgumentException
     */
    protected function parseClasses($classes)
    {
        if (! $classes) {
            return '';
        }

        if (is_string($classes)) {
            return ' ' .$classes;
        }

        $result = [];

        if (is_array($classes) || is_a($classes, \Traversable::class)) {
            foreach ($classes as $key => $value) {
                if (is_string($key)) {
                    if (is_callable($value) && $value()) {
                        $result[] = $key;
                    } else if ($value) {
                        $result[] = $key;
                    }
                } else {
                    $result[] = $value;
                }
            }
            $result = array_unique($result);
        } else if (is_callable($classes)) {
            $result[] = $classes();
        } else {
            throw new InvalidArgumentException('Invalid class list provided');
        }

        if (! $result) {
            return '';
        }

        return ' ' . implode(' ', $result);
    }

    /**
     * Normalize ID of SVG.
     *
     * @param string $id    SVG ID
     *
     * @return string
     */
    protected function normalizeSvgId($id)
    {
        $id = preg_replace('/[ \._]+/', '-', $id);
        $id = str_replace(':', '-', $id);
        return $id;
    }

    /**
     * Normalize SVG name.
     *
     * @param string $name    SVG name
     *
     * @return string
     */
    protected function normalizeSvgName($name)
    {
        if (substr($name, -4) === '.svg') {
            $name = substr($name, 0, -4);
        }

        $name = str_replace('/', '.', $name);

        return $name;
    }

    /**
     * Normalize SVG filename.
     *
     * @param string $name    SVG name
     *
     * @return string
     */
    protected function normalizeFileName($name)
    {
        return str_replace('.', '/', $name) . '.svg';
    }

    /**
     * Get RawCss asset object.
     *
     * @return RawCss
     */
    public function getCss()
    {
        return static::$css;
    }

    /**
     * Set common CSS class for all icons.
     *
     * @return void|string
     */
    public function commonCssClass($value = null)
    {
        if ($value === null) {
            return static::$CSS_CLASS;
        }
        static::$CSS_CLASS = $value;
    }

    /**
     * Set CSS class prefix.
     *
     * @return void|string
     */
    public function cssClassPrefix($value = null)
    {
        if ($value === null) {
            return static::$CSS_PREFIX;
        }
        static::$CSS_PREFIX = $value;
    }

    /**
     * Set SVG id prefix.
     *
     * @return void|string
     */
    public function svgIdPrefix($value = null)
    {
        if ($value === null) {
            return static::$ID_PREFIX;
        }
        static::$ID_PREFIX = $value;
    }

}
