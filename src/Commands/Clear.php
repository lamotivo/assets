<?php

namespace Lamotivo\Assets\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class Clear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'assets:clear'
        . ' {--d|days=0 : Age of files (in days)}'
        . ' {--fonts : Clear fonts files}'
        . ' {--css : Clear only CSS files}'
        . ' {--js : Clear only JS files}'
        . ' {--e|external : Additionally clear external assets}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear all asset cache files';


    public function handle()
    {
        $days     = $this->option('days');
        $external = $this->option('external');
        $css      = $this->option('css');
        $js       = $this->option('js');
        $fonts    = $this->option('fonts');

        $asset = resolve('asset.manager');

        if ($fonts)
        {
            $result = $asset->clearFonts($days);
        }
        else
        {
            $mask = null;

            if ($css && !$js)
            {
                $mask = 'css';
            }

            if ($js && !$css)
            {
                $mask = 'js';
            }

            $result = $asset->clear($days, $external, $mask);
        }

        if ($result)
        {
            $this->info(implode("\n", $result));
            $this->info('Total: ' . count($result));
        }
        else
        {
            $this->info('No asset cache files found.');
        }
    }


}
