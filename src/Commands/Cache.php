<?php

namespace Lamotivo\Assets\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class Cache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'assets:cache
                    {--level= : Cache level to set, "full" by default}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set the assets cache level';

    /**
     * Allowed levels list.
     *
     * @var array
     */
    protected $allowed_levels = ['none', 'basic', 'middle', 'full'];

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        if (!$level = $this->option('level'))
        {
            $level = 'full';
        }

        $this->updateCacheLevel($level);
    }

    /**
     * Update the environment file with a new cache level.
     *
     * @return void
     */
    protected function updateCacheLevel($level)
    {
        if (!in_array($level, $this->allowed_levels))
        {
            $this->error("Level must be one of \n  - " . implode("\n  - ", $this->allowed_levels));
        }
        else
        {
            $this->updateEnvFile('ASSETS_CACHE_LEVEL', $level);
            $this->info('Assets cache level is set to "'.$level.'" successfully.');
        }
    }

    /**
     * Update the given key with the given value in the environment file.
     *
     * @param  string  $key
     * @param  string  $value
     * @return void
     */
    protected function updateEnvFile($key, $value)
    {
        $env = file_get_contents($this->laravel->environmentFilePath());

        if (preg_match('/^'.$key.'=(.*)$/m', $env))
        {
            $env = preg_replace('/^'.$key.'=(.*)$/m', $key.'='.$value, $env);
        }
        else
        {
            $env .= "\n$key=$value\n";
        }

        file_put_contents($this->laravel->environmentFilePath(), $env);
    }
}
