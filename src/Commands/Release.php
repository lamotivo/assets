<?php

namespace Lamotivo\Assets\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class Release extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'assets:release
                    {--revision : Update only the revision salt}
                    {--key : Update only the cache key}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the assets revision salt and/or cache key';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->option('revision'))
        {
            $this->updateRevisionSalt();
        }
        elseif ($this->option('key'))
        {
            $this->updateCacheKey();
        }
        else
        {
            $this->updateRevisionSalt();
            $this->updateCacheKey();
        }
    }

    /**
     * Update the environment file with a new revision salt.
     *
     * @return void
     */
    protected function updateRevisionSalt()
    {
        $salt = Str::random(16);
        $this->updateEnvFile('ASSETS_REVISION_SALT', $salt);
        $this->info('Assets revision salt updated successfully.');
    }

    /**
     * Update the environment file with a new cache key.
     *
     * @return void
     */
    protected function updateCacheKey()
    {
        $key = 'assets.cache.'.Str::random(16).'.';
        $this->updateEnvFile('ASSETS_CACHE_KEY', $key);
        $this->info('Assets cache key updated successfully.');
    }

    /**
     * Update the given key with the given value in the environment file.
     *
     * @param  string  $key
     * @param  string  $value
     * @return void
     */
    protected function updateEnvFile($key, $value)
    {
        $env = file_get_contents($this->laravel->environmentFilePath());

        if (preg_match('/^'.$key.'=(.*)$/m', $env))
        {
            $env = preg_replace('/^'.$key.'=(.*)$/m', $key.'='.$value, $env);
        }
        else
        {
            $env .= "\n$key=$value\n";
        }

        file_put_contents($this->laravel->environmentFilePath(), $env);
    }
}
