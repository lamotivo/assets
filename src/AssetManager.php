<?php

namespace Lamotivo\Assets;

use Exception;
use InvalidArgumentException;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\HtmlString;
use Illuminate\Support\Facades\Cache;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Filesystem\Filesystem;

class AssetManager
{
    /**
     * Hint path delimiter value.
     *
     * @var string
     */
    const HINT_PATH_DELIMITER = '::';

    /**
     * The Filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * The array of assets that have been located.
     *
     * @var array
     */
    protected $assets = [];

    /**
     * The array of assets paths.
     *
     * @var array
     */
    protected $paths = [];

    /**
     * The namespace list to assets path hints.
     *
     * @var array
     */
    protected $hints = [];

    /**
     * CSS assets
     *
     * @var array
     */
    protected $css_assets = [
        'app' => [],
    ];

    /**
     * Javascript assets
     *
     * @var array
     */
    protected $js_assets = [
        'app' => [],
    ];

    /**
     * Predefined collections of assets.
     *
     * @var array
     */
    protected $collections = [];

    /**
     * Notifiers.
     *
     * @var array
     */
    protected $notifiers = [];


    /**
     * Font provider.
     *
     * @var string
     */
    protected $font_provider = 'google';

    /**
     * Font providers.
     *
     * @var array
     */
    protected $font_providers = [
        'google' => [
            'url' => 'https://fonts.googleapis.com/css',
        ],
    ];

    /**
     * Font subsets.
     *
     * @var array
     */
    protected $font_subsets = [];

    /**
     * Fonts.
     *
     * @var array
     */
    protected $fonts = [];

    /**
     * Font display.
     *
     * @var string
     */
    protected $font_display = '';

    /**
     * Local fonts.
     *
     * @var array
     */
    protected $local_fonts = [];


    /**
     * Temp path
     *
     * @var string
     */
    protected $temp_path = '';

    /**
     * Public path to compiled assets
     *
     * @var string
     */
    protected $public_prefix = 'lm-assets';

    /**
     * Public url (if usign CDN)
     *
     * @var string
     */
    protected $public_url = '';

    /**
     * Css filters
     *
     * @var array
     */
    protected $css_filters = [];

    /**
     * Js filters
     *
     * @var array
     */
    protected $js_filters = [];

    /**
     * Scss formatter
     *
     * @var string
     */
    protected $scss_formatter = 'compact';

    /**
     * Scss paths
     *
     * @var array
     */
    protected $scss_paths = [];

    /**
     * Scss variables
     *
     * @var array
     */
    protected $scss_vars = [];

    /**
     * Scss functions
     *
     * @var array
     */
    protected $scss_functions = [];

    /**
     * Cache level
     *
     * @var string
     */
    protected $cache_level = 'basic';

    /**
     * Cache key prefix
     *
     * @var string
     */
    protected $cache_prefix = 'assets.cache.';

    /**
     * Revision salt
     *
     * @var string
     */
    protected $revision_salt = '';

    /**
     * Cache store
     *
     * @var string
     */
    protected $cache_store = null;


    /**
     * Create a new AssetManager instance.
     *
     * @param  array  $config
     * @param  \Illuminate\Filesystem\Filesystem  $files
     * @return void
     */
    public function __construct($config, Filesystem $files)
    {
        $this->files = $files;

        if (!empty($config['paths']))
        {
            foreach ($config['paths'] as $path)
            {
                $this->addLocation($path);
            }            
        }

        $this->cache_level   = Arr::get($config, 'cache_level', 'basic');
        $this->cache_prefix  = Arr::get($config, 'cache_key', 'assets.cache.');
        $this->cache_store   = Arr::get($config, 'cache_store', config('cache.default'));

        $this->revision_salt = Arr::get($config, 'revision_salt', '');

        $this->public_prefix = trim(Arr::get($config, 'public_prefix', 'lm-assets'), '/');
        $this->public_url    = rtrim(Arr::get($config, 'public_url', ''), '/');
        $this->temp_path     = rtrim(Arr::get($config, 'temp_path', storage_path('temp')), '/');

        $this->css_filters   = Arr::get($config, 'css_filters', []);
        $this->js_filters    = Arr::get($config, 'js_filters', []);
        $this->collections   = Arr::get($config, 'collections', []);
        $this->notifiers     = Arr::get($config, 'notifiers', []);
        $this->local_fonts   = Arr::get($config, 'local_fonts', []);
        $this->fonts         = Arr::get($config, 'fonts', []);
        $this->font_subsets  = Arr::get($config, 'font_subsets', []);
        $this->font_display  = Arr::get($config, 'font_display', '');

        if ($font_providers = Arr::get($config, 'font_providers')) {
            $this->font_providers = $font_providers;
        }

        if ($font_provider = Arr::get($config, 'font_provider')) {
            if (isset($this->font_providers[$font_provider])) {
                $this->font_provider = $font_provider;
            }
        }

        if (!empty($config['scss']))
        {
            $this->scss_formatter = Arr::get($config['scss'], 'formatter', 'compact');
            $this->scss_paths = Arr::get($config['scss'], 'paths', []);
            $this->scss_vars = Arr::get($config['scss'], 'vars', []);
            $this->scss_functions = Arr::get($config['scss'], 'functions', []);
        }

        if (!file_exists(public_path($this->public_prefix)))
        {
            @mkdir(public_path($this->public_prefix), 0777, true);
        }

        if (!file_exists($this->temp_path))
        {
            @mkdir($this->temp_path, 0777, true);
        }
    }

    /**
     * Shortcut for IconManager.
     *
     * @return IconManager
     */
    public function icons()
    {
        return app('icon.manager');
    }

    /**
     * Get RawCss asset object for processed SVG icons.
     *
     * @return RawCss
     */
    public function iconCss()
    {
        return $this->icons()->getCss();
    }

    /**
     * Add an icon to IconManager and pass it to the HTML code.
     *
     * @param string $svgFileName               SVG path
     * @param string|array|\Closure $classes    Classes appended to SVG tag
     *
     * @return HtmlString
     *
     * @throws InvalidArgumentException
     */
    public function icon($svgFileName, $classes = null)
    {
        return $this->icons()->icon($svgFileName, $classes);
    }

    /**
     * Add an icon or icons to the IconMap.
     *
     * @param string|array $icons     Icons
     *
     * @return void
     *
     * @throws InvalidArgumentException
     */
    public function addIcons($icons)
    {
        return $this->icons()->add($icons);
    }

    /**
     * Add a raw SVG icon to IconManager.
     *
     * @param string $id        Icon ID
     * @param string $svg       SVG code
     * @param boolean $force    Force to replace an existing icon
     *
     * @return boolean
     */
    public function addRawSvg($id, $svg, $force = false)
    {
        return $this->icons()->pushIcon($id, $svg, $force);
    }

    /**
     * Replace a raw SVG icon.
     *
     * @param string $id        Icon ID
     * @param string $svg       SVG code
     *
     * @return boolean
     */
    public function replaceRawSvg($id, $svg)
    {
        return $this->icons()->pushIcon($id, $svg, true);
    }

    /**
     * Add a collection.
     *
     * @param  string  $name
     * @param  mixed   $collection
     * @return AssetManager
     */
    public function addCollection($name, $collection)
    {
        $this->collections[$name] = $collection;
        return $this;
    }

    /**
     * Get collections.
     *
     * @return array
     */
    public function getCollections()
    {
        return $this->collections;
    }

    /**
     * Add a location.
     *
     * @param  string  $location
     * @return AssetManager
     */
    public function addLocation($location)
    {
        $this->paths[] = $location;
        return $this;
    }

    /**
     * Prepend a location.
     *
     * @param  string  $location
     * @return AssetManager
     */
    public function prependLocation($location)
    {
        array_unshift($this->paths, $location);
        return $this;
    }

    /**
     * Get locations.
     *
     * @return array
     */
    public function getLocations()
    {
        return $this->paths;
    }

    /**
     * Add a namespace hint.
     *
     * @param  string  $namespace
     * @param  string|array  $hints
     * @return void
     */
    public function addNamespace($namespace, $hints)
    {
        $hints = (array)$hints;
        if (isset($this->hints[$namespace]))
        {
            $hints = array_merge($this->hints[$namespace], $hints);
        }
        $this->hints[$namespace] = $hints;
    }

    /**
     * Prepend a namespace hint.
     *
     * @param  string  $namespace
     * @param  string|array  $hints
     * @return void
     */
    public function prependNamespace($namespace, $hints)
    {
        $hints = (array)$hints;
        if (isset($this->hints[$namespace]))
        {
            $hints = array_merge($hints, $this->hints[$namespace]);
        }
        $this->hints[$namespace] = $hints;
    }

    /**
     * Replace the namespace hints for the given namespace.
     *
     * @param  string  $namespace
     * @param  string|array  $hints
     * @return void
     */
    public function replaceNamespace($namespace, $hints)
    {
        $this->hints[$namespace] = (array)$hints;
    }

    /**
     * Returns whether or not the asset name is namespaced.
     *
     * @param  string  $name
     * @return bool
     */
    public function isNamespaced($name)
    {
        return strpos($name, static::HINT_PATH_DELIMITER) > 0;
    }

    /**
     * Get the namespace to asset hints.
     *
     * @return array
     */
    public function getHints()
    {
        return $this->hints;
    }

    /**
     * Add a font provider.
     *
     * @param  string  $name
     * @param  array  $config
     * @return AssetManager
     */
    public function addFontProvider($name, $config)
    {
        $this->font_providers[$name] = $config;

        $this->useFontProvider($name);

        return $this;
    }

    /**
     * Use the specified font provider.
     *
     * @param  string  $name
     * @return AssetManager
     */
    public function useFontProvider($name)
    {
        if (isset($this->font_providers[$name]['url'])) {
            $this->font_provider = $name;
        }

        return $this;
    }

    /**
     * Get the used font provider URL.
     *
     * @return string
     */
    public function getFontProviderUrl()
    {
        $name = $this->font_provider;

        if (isset($this->font_providers[$name]['url'])) {
            return $this->font_providers[$name]['url'];
        }

        return 'https://fonts.googleapis.com/css';
    }

    /**
     * Reset fonts.
     *
     * @return AssetManager
     */
    public function resetFonts()
    {
        $this->fonts = [];

        return $this;
    }

    /**
     * Add a font.
     *
     * @param  string  $family
     * @param  string  $variants
     * @param  string  $subset
     * @return AssetManager
     */
    public function addFont($family, $variants = null, $subset = null)
    {
        $this->fonts[$family] = $variants;

        $this->addFontSubset($subset);

        return $this;
    }

    /**
     * Add a font subset.
     *
     * @param  string  $subset
     * @return AssetManager
     */
    public function addFontSubset($subset)
    {
        if ($subset && !in_array($subset, $this->font_subsets))
        {
            $this->font_subsets[] = $subset;
        }

        return $this;
    }

    /**
     * Add a local font.
     *
     * @param  string  $family
     * @param  array   $variants
     * @return AssetManager
     */
    public function addLocalFont($family, array $variants = null)
    {
        $this->local_fonts[$family] = $variants;

        return $this;
    }

    /**
     * Get public prefix.
     *
     * @return string
     */
    public function getPublicPrefix()
    {
        return $this->public_prefix;
    }

    /**
     * Reset assets.
     *
     * @return AssetManager
     */
    public function reset($group = 'app')
    {
        $this->js_assets[$group] = [];
        $this->css_assets[$group] = [];
        return $this;
    }

    /**
     * Reset JS assets.
     *
     * @return AssetManager
     */
    public function resetJs($group = 'app')
    {
        $this->js_assets[$group] = [];
        return $this;
    }

    /**
     * Reset CSS assets.
     *
     * @return AssetManager
     */
    public function resetCss($group = 'app')
    {
        $this->css_assets[$group] = [];
        return $this;
    }

    /**
     * Add one or more assets.
     *
     * @param string|string[]|RawAsset $source A local filename, a remote URL, a raw asset or a collection name
     * @param string          $group Assets group
     *
     * @return AssetManager
     */
    public function add($source, $group = 'app')
    {
        $this->checkGroup($group);

        if (is_array($source))
        {
            foreach ($source as $asset)
            {
                $this->add($asset, $group);
            }
        }
        elseif (!is_string($source))
        {
            if (is_a($source, RawAsset::class))
            {
                $this->addRawAsset($source, $group);
            }
        }
        elseif (array_key_exists($source, $this->collections))
        {
            $this->add($this->collections[$source], $group);
        }
        else
        {
            $action = 'addJs';

            switch (pathinfo($source, PATHINFO_EXTENSION))
            {
                case 'css':
                case 'scss':
                    $action = 'addCss';
                    break;
            }

            $this->$action($source, $group);
        }

        return $this;
    }

    /**
     * Add one or more Javascript assets.
     *
     * @param string|string[]|RawAsset $source
     * @param string          $group
     *
     * @return AssetManager
     */
    public function addJs($source, $group = 'app')
    {
        $this->checkGroup($group);

        if (is_array($source))
        {
            foreach ($source as $asset)
            {
                $this->addJs($asset, $group);
            }
        }
        elseif (!is_string($source))
        {
            if (is_a($source, RawAsset::class))
            {
                $this->addRawAsset($source, $group);
            }
        }
        elseif (array_key_exists($source, $this->collections))
        {
            $this->addJs($this->collections[$source], $group);
        }
        else
        {
            if ( ! in_array($source, $this->js_assets[$group]))
            {
                $this->js_assets[$group][] = $source;
            }
        }

        return $this;
    }

    /**
     * Add one or more Stylesheet assets.
     *
     * @param string|string[]|RawAsset $source
     * @param string          $group
     *
     * @return AssetManager
     */
    public function addCss($source, $group = 'app')
    {
        $this->checkGroup($group);

        if (is_array($source))
        {
            foreach ($source as $asset)
            {
                $this->addCss($asset, $group);
            }
        }
        elseif (!is_string($source))
        {
            if (is_a($source, RawAsset::class))
            {
                $this->addRawAsset($source, $group);
            }
        }
        elseif (array_key_exists($source, $this->collections))
        {
            $this->addCss($this->collections[$source], $group);
        }
        else
        {
            if ( ! in_array($source, $this->css_assets[$group]))
            {
                $this->css_assets[$group][] = $source;
            }
        }

        return $this;
    }

    /**
     * Add a RawAsset.
     *
     * @param string|string[]|RawAsset $asset
     * @param string          $group
     *
     * @return AssetManager
     */
    public function addRawAsset(RawAsset $asset, $group = 'app')
    {
        $this->checkGroup($group);

        if (is_a($asset, RawCss::class))
        {
            if ( ! in_array($asset, $this->css_assets[$group]))
            {
                $this->css_assets[$group][] = $asset;
            }
        }
        else if (is_a($asset, RawJs::class))
        {
            if ( ! in_array($asset, $this->js_assets[$group]))
            {
                $this->js_assets[$group][] = $asset;
            }
        }

        return $this;
    }

    /**
     * Add a raw asset as SCSS.
     *
     * @param string $content
     * @param string $group
     *
     * @return RawScss
     */
    public function addRawScss($content, $group = 'app')
    {
        $asset = new RawScss($content);
        $this->addRawAsset($asset, $group);
        return $asset;
    }

    /**
     * Add a raw asset as CSS.
     *
     * @param string $content
     * @param string $group
     *
     * @return RawCss
     */
    public function addRawCss($content, $group = 'app')
    {
        $asset = new RawCss($content);
        $this->addRawAsset($asset, $group);
        return $asset;
    }

    /**
     * Add a raw asset as JavaScript.
     *
     * @param string $content
     * @param string $group
     *
     * @return RawJs
     */
    public function addRawJs($content, $group = 'app')
    {
        $asset = new RawJs($content);
        $this->addRawAsset($asset, $group);
        return $asset;
    }

    /**
     * Render the CSS assets.
     *
     * @param string   $group      Assets group
     * @param string[] $attributes Optional attributes
     *
     * @return string
     */
    public function css($group = 'app', array $attributes = [])
    {
        $this->checkGroup($group);

        return $this->render($this->css_assets[$group], '.css', $group, $attributes);
    }

    /**
     * Render the JS assets.
     *
     * @param string   $group      Assets group
     * @param string[] $attributes Optional attributes
     *
     * @return string
     */
    public function js($group = 'app', array $attributes = [])
    {
        $this->checkGroup($group);

        return $this->render($this->js_assets[$group], '.js', $group, $attributes);
    }

    /**
     * Render the Fonts imports.
     *
     * @return string
     */
    public function fonts()
    {
        $families = [];
        $local_families = [];

        foreach ($this->fonts as $family => $variants)
        {
            if (is_int($family))
            {
                $family = $variants;
                $variants = '400';
            }

            if (isset($this->local_fonts[$family]))
            {
                $local_families[] = [$family, ($variants?:'400')];
            }
            else
            {
                $families[] = urlencode($family) . ':' . ($variants?:'400');
            }
        }

        $style = [];

        if ($families)
        {
            $url = $this->getFontProviderUrl() . '?family=' . implode('|', $families);

            if ($subsets = implode(',', $this->font_subsets))
            {
                $url .= '&subset=' . $subsets;
            }

            if ($this->font_display)
            {
                $url .= '&display=' . $this->font_display;
            }

            $style[] = '<style>@import url("' . $url . '");</style>';
        }

        if ($local_families)
        {
            $hash = $this->hash(serialize([$local_families, $this->font_display]));
            $asset_file = $this->public_prefix . '/fonts-' . $hash . '.css';

            if ( ! file_exists(public_path($asset_file)))
            {
                $css = [];

                foreach ($local_families as $local_family)
                {
                    $family = $local_family[0];
                    $variants = $local_family[1];
                    $variants = explode(',', $variants);
                    foreach ($variants as $variant)
                    {
                        $font_files = $this->discoverFont($family, $variant);
                        $css[] = $this->renderFontCss($family, $variant, $font_files);
                    }
                }

                file_put_contents(public_path($asset_file), implode("\n", $css));
            }

            $style[] = '<style>@import url("' . url($asset_file) . '");</style>';
        }

        return implode("\n", $style);
    }

    /**
     * @var array
     */
    protected $font_formats = [
        'eot'   => 'embedded-opentype',
        'woff'  => 'woff',
        'woff2' => 'woff2',
        'ttf'   => 'truetype',
    ];

    protected function discoverFont($family, $variant = '400')
    {
        if (! isset($this->local_fonts[$family][$variant])) {
            throw new InvalidArgumentException('Font [' . $family . ':' . $variant .'] not found.');
        }
        $paths = [];
        $baseAssetName = Str::slug($this->local_fonts[$family][$variant]);
        foreach ($this->font_formats as $ext => $format) {
            try {
                $path = $this->findAsset('fonts/' . $baseAssetName . '.' . $ext);
                $hash = $this->hash($path . filemtime($path));
                $asset_file = $this->public_prefix . '/font-' . $hash . '.' . $ext;
                if ( ! file_exists(public_path($asset_file)))
                {
                    copy($path, public_path($asset_file));
                }
                $paths[$ext] = $asset_file;
            } catch (Exception $e) { }
        }

        if (!$paths) {
            throw new InvalidArgumentException('Font [' . $baseAssetName . '] not found.');
        }

        return $paths;
    }

    protected function renderFontCss($family, $variant, array $files)
    {
        if (! isset($this->local_fonts[$family][$variant])) {
            throw new InvalidArgumentException('Font [' . $family . ':' . $variant .'] not found.');
        }

        $fontName = $this->local_fonts[$family][$variant];

        $css = [];
        $css[] = '  font-family: "' . $family . '";';
        $css[] = '  font-style: ' . (Str::endsWith($variant, 'i') ? 'italic' : 'normal' ) . ';';
        $css[] = '  font-weight: ' . trim($variant, 'i') . ';';

        if ($this->font_display)
        {
            $css[] = '  font-display: ' . $this->font_display . ';';
        }

        if (isset($files['eot']))
        {
            $css[] = '  src: url("' . basename($files['eot']) . '");';
        }

        $src = [];

        $src[] = 'local("' . $fontName . '")';
        if (isset($files['eot']))
        {
            $src[] = 'url("' . basename($files['eot']) . '?#iefix") format("' . $this->font_formats['eot'] . '")';
        }
        if (isset($files['woff']))
        {
            $src[] = 'url("' . basename($files['woff']) . '") format("' . $this->font_formats['woff'] . '")';
        }
        if (isset($files['woff2']))
        {
            $src[] = 'url("' . basename($files['woff2']) . '") format("' . $this->font_formats['woff2'] . '")';
        }
        if (isset($files['ttf']))
        {
            $src[] = 'url("' . basename($files['ttf']) . '") format("' . $this->font_formats['ttf'] . '")';
        }

        $css[] = '  src: ' . implode(', ', $src) . ';';

        return "@font-face {\n" . implode("\n", $css) . "\n}\n";
    }

    /**
     * Get SCSS formatter.
     *
     * @return array
     */
    public function getScssFormatter()
    {
        return $this->scss_formatter;
    }

    /**
     * Get SCSS paths.
     *
     * @return array
     */
    public function getScssPaths()
    {
        return $this->scss_paths;
    }

    /**
     * Get SCSS variables.
     *
     * @return array
     */
    public function getScssVars()
    {
        return $this->scss_vars;
    }

    /**
     * Add a SCSS variable.
     *
     * @param  string  $variable
     * @param  string  $value
     * @return AssetManager
     */
    public function addScssVar($variable, $value)
    {
        $this->scss_vars[$variable] = $value;
        return $this;
    }

    /**
     * Add multiple SCSS variables.
     *
     * @param  array  $map
     * @return AssetManager
     */
    public function addScssVars($map)
    {
        foreach ($map as $variable => $value)
        {
            $this->scss_vars[$variable] = $value;
        }
        return $this;
    }

    /**
     * Reset SCSS variables.
     *
     * @return AssetManager
     */
    public function resetScssVars()
    {
        $this->scss_vars = [];
        return $this;
    }

    /**
     * Get SCSS functions.
     *
     * @return array
     */
    public function getScssFunctions()
    {
        return $this->scss_functions;
    }

    /**
     * Get SCSS import function.
     *
     * @return callable
     */
    public function getScssImportFunction()
    {
        return function($path) {
            try
            {
                if ( ! Str::endsWith($path, ['.scss', '.css']))
                {
                    $path = $path . '.scss';
                }
                return $this->findAsset($path);
            }
            catch (Exception $e)
            {
                try
                {
                    $dir = dirname($path);
                    $base = basename($path);
                    if ( ! Str::startsWith($base, '_'))
                    {
                        $path = $dir . '/_' . $base;
                        return $this->findAsset($path);
                    }

                    return null;
                }
                catch (Exception $e)
                {
                    return null;
                }
            }
        };
    }

    /**
     * Find asset file.
     * Asset file can be located at the filesystem root, 
     * at the application base path, or at a custom path 
     * from the predefined paths list.
     *
     * @param string    $asset_file
     *
     * @return string
     */
    public function findAsset($asset_file)
    {
        if (isset($this->assets[$asset_file])) {
            return $this->assets[$asset_file];
        }

        $asset_file = trim($asset_file);

        if ($this->isNamespaced($asset_file)) {
            return $this->assets[$asset_file] = $this->findNamespacedAsset($asset_file);
        }

        if (file_exists($asset_file)) {
            return $this->assets[$asset_file] = $asset_file;
        }

        if (file_exists(base_path($asset_file))) {
            return $this->assets[$asset_file] = base_path($asset_file);
        }

        return $this->assets[$asset_file] = $this->findInPaths($asset_file, $this->paths);
    }

    /**
     * Get the path to an asset file with a namespaced path.
     *
     * @param  string  $asset_file
     * @return string
     */
    protected function findNamespacedAsset($asset_file)
    {
        list($namespace, $asset_file) = $this->parseNamespaceSegments($asset_file);
        return $this->findInPaths($asset_file, $this->hints[$namespace]);
    }

    /**
     * Get the segments of an asset file with a namespaced path.
     *
     * @param  string  $asset_file
     * @return array
     *
     * @throws \InvalidArgumentException
     */
    protected function parseNamespaceSegments($asset_file)
    {
        $segments = explode(static::HINT_PATH_DELIMITER, $asset_file);
        if (count($segments) !== 2) {
            throw new InvalidArgumentException('Asset [' . $asset_file . '] has an invalid name.');
        }
        if (! isset($this->hints[$segments[0]])) {
            throw new InvalidArgumentException('No hint path defined for [' . $segments[0] . '].');
        }
        return $segments;
    }

    /**
     * Find the given asset in the list of paths.
     *
     * @param  string  $asset_file
     * @param  array   $paths
     * @return string
     *
     * @throws \InvalidArgumentException
     */
    protected function findInPaths($asset_file, $paths)
    {
        foreach ((array)$paths as $path) {
            if (file_exists($path . '/' . $asset_file)) {
                return $path . '/' . $asset_file;
            }
        }
        throw new InvalidArgumentException('Asset [' . $asset_file . '] not found.');
    }

    /**
     * Render markup to load the CSS or JS assets.
     *
     * @param string[]          $assets        The assets files
     * @param string            $extension     ".css" or ".js"
     * @param string[]          $attributes    Optional attributes
     *
     * @return string
     */
    protected function render(array $assets, $extension, $group, array $attributes = [])
    {
        $temp_files = [];

        $filters = [];

        $full_hash = '';

        $format_link = '';

        switch ($extension) {
            case '.css':
            case '.scss':
                $format_link = '<link rel="stylesheet" href="%s"%s>';
                $filters = $this->css_filters;
                break;

            case '.js':
                $format_link = '<script src="%s"%s></script>';
                $filters = $this->js_filters;
                break;
        }

        $hash_key = '';

        if ($this->cache_level === 'full')
        {
            $hash_key = $this->cache_prefix . $this->hash(implode('', $assets));

            $asset_file = $this->getCache($hash_key);

            if ($asset_file && file_exists(public_path($asset_file)))
            {
                return $this->renderLinkForFile($asset_file, $format_link, $attributes);
            }
        }

        foreach ($assets as $asset)
        {
            $absoluteUrl = false;
            $path = '';
            $hash = '';
            $temp_file = '';
            $data = null;

            if (!is_string($asset)) {
                if (is_a($asset, RawAsset::class)) {
                    $path = $asset;
                    $hash = $asset->hash();
                    $temp_file = $this->temp_path . '/asset-raw-' . $hash . $extension;
                    $data = $asset->content();
                }
            } else {
                $absoluteUrl = preg_match('/^((https?:)?\/\/|data:)/i', $asset);

                if ($absoluteUrl)
                {
                    $path = $asset;
                    $hash = $this->hash($path, 32, false);
                    $temp_file = $this->temp_path . '/external-' . $hash . $extension;
                }
                else
                {
                    $path = $this->findAsset($asset);
                    if ($this->cache_level === 'middle' || $this->cache_level === 'full')
                    {
                        $hash = $this->hash($path);
                    }
                    else
                    {
                        $hash = $this->hash($path . filemtime($path));
                    }
                    $temp_file = $this->temp_path . '/asset-' . $hash . $extension;
                }
            }

            if ( ! file_exists($temp_file) || ( ! $absoluteUrl && $this->cache_level === 'none'))
            {
                if (!$data && is_string($path)) {
                    $data = file_get_contents($path);
                }

                foreach ($filters as $filter)
                {
                    $data = $filter->filter($data, $path, $this);
                }

                file_put_contents($temp_file, $data);
            }

            $temp_files[] = $temp_file;
        }

        $full_hash = $this->hash(implode('', $temp_files), 10);

        $asset_file = $this->public_prefix . '/' . $group . '-' . $full_hash . $extension;

        $this->combineFiles($asset_file, $temp_files);

        if ($hash_key)
        {
            $this->saveCache($hash_key, $asset_file);
        }

        return $this->renderLinkForFile($asset_file, $format_link, $attributes);
    }

    /**
     * Get a value from cache for the given key.
     *
     * @param string $key
     * @return mixed
     */
    protected function getCache($key)
    {
        return $this->cache_store ? Cache::store($this->cache_store)->get($key) : null;
    }

    /**
     * Save the value to cache with the given key.
     *
     * @param string $key
     * @param mixed $value
     * @return void
     */
    protected function saveCache($key, $value)
    {
        if ($this->cache_store)
        {
            Cache::store($this->cache_store)->forever($key, $value);
        }
    }

    /**
     * Render HTML link to an asset file.
     *
     * @param string    $asset_file
     * @param string    $format_link
     * @param string[]  $attributes    Optional attributes
     */
    protected function renderLinkForFile($asset_file, $format_link, array $attributes = [])
    {
        if ($this->public_url)
        {
            $url = $this->public_url . '/' . $asset_file;
        }
        else
        {
            $url = url($asset_file);
        }

        return $this->renderLink($url, $format_link, $attributes);
    }

    /**
     * Checking the assets group.
     *
     * @param string $group
     */
    protected function checkGroup($group)
    {
        if ( ! isset($this->css_assets[$group]))
        {
            $this->css_assets[$group] = [];
        }
        if ( ! isset($this->js_assets[$group]))
        {
            $this->js_assets[$group] = [];
        }
    }

    /**
     * Combine files.
     *
     * @param string   $asset_file  Asset filename
     * @param string[] $files       Files to combines
     */
    protected function combineFiles($asset_file, $files)
    {
        if ( ! file_exists(public_path($asset_file)))
        {
            $data = '';

            foreach ($files as $file)
            {
                $data .= file_get_contents($file);
            }

            file_put_contents(public_path($asset_file), $data);

            foreach ($this->notifiers as $notifier)
            {
                $notifier->created(public_path($asset_file));
            }
        }
    }

    /**
     * Generate a hash, to use as a filename for generated assets.
     *
     * @param string $text
     * @param int $length
     *
     * @return string
     */
    protected function hash($text, $length = 32, $use_revision = true)
    {
        $salt = $use_revision ? $this->revision_salt : '';
        return substr(md5($salt . $text), 0, $length);
    }


    /**
     * Generate HTML link to an asset file.
     *
     * @param string   $url       path to the asset file
     * @param string   $format
     * @param string[] $attributes
     *
     * @return string
     */
    protected function renderLink($url, $format, $attributes)
    {
        $attributes = $this->prepareAttributes($attributes);

        return new HtmlString(sprintf($format, $url, $attributes));
    }

    /**
     * Convert an array of attributes to HTML.
     *
     * @param string[] $array
     *
     * @return string
     */
    protected function prepareAttributes(array $array)
    {
        $attributes = [];

        foreach ($array as $key => $value)
        {
            if (is_int($key))
            {
                $attributes[] = $value;
            }
            else
            {
                $attributes[] = $key . '="' . e($value) . '"';
            }
        }

        return $attributes ? ' ' . implode(' ', $attributes) : '';
    }

    /**
     * Clear generated assets older than a given number of days
     *
     * @param int $days
     * @param boolean $external
     * @param string $mask
     */
    public function clear($days = 0, $external = false, $mask = null)
    {
        if ( ! $mask)
        {
            $mask = '{js,css}';
        }

        $files = glob(public_path($this->public_prefix) . '/*.' . $mask, GLOB_BRACE);
        $files = array_merge($files, glob($this->temp_path . '/asset-*.' . $mask, GLOB_BRACE));

        if ($external)
        {
            $files = array_merge($files, glob($this->temp_path . '/external-*.' . $mask, GLOB_BRACE));
        }

        $ts = time() - $days * 86400;

        $deleted = [];

        foreach ($files as $file)
        {
            if (filemtime($file) <= $ts)
            {
                $deleted[] = 'Deleted: ' . $file;
                unlink($file);
            }
        }

        return $deleted;
    }

    /**
     * Clear generated fonts older than a given number of days
     *
     * @param int $days
     * @param boolean $external
     * @param string $mask
     */
    public function clearFonts($days = 0)
    {
        $files = glob(public_path($this->public_prefix) . '/font-*.{eot,woff,woff2,ttf,svg}', GLOB_BRACE);
        $files = array_merge($files, glob(public_path($this->public_prefix) . '/fonts-*.css'));

        $ts = time() - $days * 86400;

        $deleted = [];

        foreach ($files as $file)
        {
            if (filemtime($file) <= $ts)
            {
                $deleted[] = 'Deleted: ' . $file;
                unlink($file);
            }
        }

        return $deleted;
    }
}
