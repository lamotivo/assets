<?php

namespace Lamotivo\Assets\Notifiers;

use Lamotivo\Assets\AssetManager;

abstract class Notifier implements NotifierContract
{
    /**
     * This function is called whenever an asset file is created.
     *
     * @param string  $asset        The filename of the asset relative to public_path.
     * @param AssetManager $asset   The asset manager
     */
    public function created($asset_file, $asset)
    {
        // $full_asset_filename = public_path($asset_file);

        // Upload it somewhere...
    }

    /**
     * Allow serialization for Laravel config cache
     *
     * @return static
     */
    public static function __set_state()
    {
        return new static;
    }
}
