<?php

namespace Lamotivo\Assets\Notifiers;

use Lamotivo\Assets\AssetManager;

interface NotifierContract
{
    /**
     * This function is called whenever an asset file is created.
     *
     * @param string  $asset        The filename of the asset relative to public_path.
     * @param AssetManager $asset   The asset manager
     */
    public function created($asset_file, $asset);

    /**
     * Allow the object to be serialized in laravel's config cache
     *
     * @return static
     */
    public static function __set_state();
}
