<?php

namespace Lamotivo\Assets;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class AssetServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * @var array
     */
    protected $commands = [
        'Lamotivo\Assets\Commands\Clear',
        'Lamotivo\Assets\Commands\Release',
        'Lamotivo\Assets\Commands\Cache',
    ];

    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole())
        {
            $this->publishes([
                __DIR__ . '/../config/assets.php' => config_path('assets.php'),
            ], 'config');
        }

        $this->app->make('asset.manager')->addNamespace('bs3', __DIR__ . '/../resources/bs3');
        $this->app->make('asset.manager')->addNamespace('bs4', __DIR__ . '/../resources/bs4');
        $this->app->make('asset.manager')->addNamespace('bs5', __DIR__ . '/../resources/bs5');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/assets.php', 'assets');

        $this->registerAssetManager();
        $this->registerBladeDirectives();
        $this->registerIconManager();
        $this->commands($this->commands);
    }


    /**
     * Register the Lamotivo Asset instance.
     *
     * @return void
     */
    protected function registerAssetManager()
    {
        $this->app->singleton('asset.manager', function ($app) {
            $config = $app['config']->get('assets');

            $assetManager = new AssetManager($config, $app['files']);

            return $assetManager;
        });
    }

    /**
     * Register Blade directives.
     *
     * @return void
     */
    protected function registerBladeDirectives()
    {
        Blade::directive('asset_add', function($params) {
            return "<?php app('asset.manager')->add($params) ?>\n";
        });

        Blade::directive('asset_add_js', function($params) {
            return "<?php app('asset.manager')->addJs($params) ?>\n";
        });

        Blade::directive('asset_add_css', function($params) {
            return "<?php app('asset.manager')->addCss($params) ?>\n";
        });

        Blade::directive('asset_js', function($params) {
            return "<?= app('asset.manager')->js($params) ?>\n";
        });

        Blade::directive('asset_css', function($params) {
            return "<?= app('asset.manager')->css($params) ?>\n";
        });

        Blade::directive('asset_fonts', function($params) {
            return "<?= app('asset.manager')->fonts($params) ?>\n";
        });
    }

    /**
     * Register the Icon Manager instance.
     *
     * @return void
     */
    protected function registerIconManager()
    {
        $this->app->singleton('icon.manager', function ($app) {
            return new IconManager(app('asset.manager'));
        });

        Blade::directive('asset_icon', function($params) {
            return "<?= app('icon.manager')->icon($params) ?>\n";
        });

        Blade::directive('asset_icons', function($params) {
            return "<?= app('icon.manager')->map($params) ?>\n";
        });
    }


    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['asset.manager', 'icon.manager'];
    }
}

