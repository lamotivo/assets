<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Cache Level
    |--------------------------------------------------------------------------
    |
    | In production mode you may want to use higher cache boost to speed up
    | the perfomance of your application.
    |
    | There is 3 levels:
    |
    | - none - all asset files will be recompiled every time
    |          (CAUTION: WORST PERFORMANCE!)
    |
    | - basic - compiled asset files wil be recompiled on any changes
    |
    | - middle - once compiled, all modifications are ignored,
    |           but if an asset appears in another path, it will be recompiled
    |
    | - full - once resolved (found) and compiled, all changes are ignored
    |          (best performance)
    |          Note: when you have changes with this level and want to
    |          recompile all the stuff, consider changing the cache_key option
    |          to some new value.
    |
    | When set to 'basic', 'middle' or 'full', you can reset cache manually:
    |
    |     php artisan assets:clear
    |
    */

    'cache_level' => env('ASSETS_CACHE_LEVEL', 'basic'),

    'cache_key' => env('ASSETS_CACHE_KEY', 'assets.cache.'),

    'cache_store' => env('ASSETS_CACHE_STORE'),

    /*
    |--------------------------------------------------------------------------
    | Revision salt
    |--------------------------------------------------------------------------
    |
    | This option allows you to adjust the revision hash generation by using
    | a salt.
    |
    | It is recommended to update the salt each time you have released|deployed 
    | your project.
    |
    | You may update the salt with the command:
    |
    |     php artisan assets:release
    |
    */

    'revision_salt' => env('ASSETS_REVISION_SALT', ''),

    /*
    |--------------------------------------------------------------------------
    | Paths for finding asset sources
    |--------------------------------------------------------------------------
    |
    | This option allows you to define different locations where we find your
    | asset sources.
    |
    | You also can append other locations in runtime using
    | Asset::addLocation($some_new_path).
    |
    */

    'paths' => [
        // resource_path('assets'),
        resource_path('js'),
        resource_path('css'),
        resource_path('icons'),
    ],

    /*
    |--------------------------------------------------------------------------
    | URL prefix for you compiled assets
    |--------------------------------------------------------------------------
    |
    | This is a public directory to store and fetch your compiled asset files.
    | If it does not exists in your public path, it will be created 
    | autmatically.
    |
    | This directory should be gitignored when using GIT as a repository.
    |
    */

    'public_prefix' => env('ASSETS_PREFIX', 'lm-assets'),

    /*
    |--------------------------------------------------------------------------
    | Temp path for your prepared assets
    |--------------------------------------------------------------------------
    |
    | When we process asset files, we store a cache copy of your asset files
    | into this directory.
    |
    */

    'temp_path' => storage_path('temp/assets'),

    /*
    |--------------------------------------------------------------------------
    | Public URL for your assets
    |--------------------------------------------------------------------------
    |
    | Usually you fetch your compiled assets from your current domain.
    | But sometimes you may want to fetch it from another location (CDN or so).
    | Here you can specify the qualified URL for that location.
    | Leave blank, if you do not use other location.
    |
    */

    'public_url' => env('ASSETS_PUBLIC_URL', ''),

    /*
    |--------------------------------------------------------------------------
    | Google Font Subsets
    |--------------------------------------------------------------------------
    |
    | This list contains all font subsets to be included with fonts.
    |
    */

    'font_subsets' => [
        // 'cyrillic',
        // 'cyrillic-ext',
        // 'greek',
        // 'greek-ext',
        // 'latin-ext',
        // 'thai',
        // 'vietnamese',
    ],

    /*
    |--------------------------------------------------------------------------
    | Font Display
    |--------------------------------------------------------------------------
    |
    | Set it to "swap" to use `display: swap`.
    |
    */

    'font_display' => '',

    /*
    |--------------------------------------------------------------------------
    | Font Provider
    |--------------------------------------------------------------------------
    |
    | The default font provider.
    |
    */

    'font_provider' => env('ASSETS_FONT_PROVIDER', 'google'),

    /*
    |--------------------------------------------------------------------------
    | Font Providers
    |--------------------------------------------------------------------------
    |
    | Add font providers that will be available to use.
    |
    */

    'font_providers' => [
        'google' => [
            'url' => 'https://fonts.googleapis.com/css',
        ],
        'bunny' => [
            'url' => 'https://fonts.bunny.net/css',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Google Fonts
    |--------------------------------------------------------------------------
    |
    | Add some Google Fonts to your page.
    |
    */

    'fonts' => [
        'Roboto Condensed'       => '400,700',
        // 'Roboto'              => '400,700',
        // 'Open Sans'           => '400,700',
        // 'Open Sans Condensed' => '300,700',
        // 'Montserrat'          => '400,700',
        // 'Noto Sans'           => '400,700',
        // 'PT Sans'             => '400,700',
        // 'PT Sans Narrow'      => '400,700',
        // 'Source Sans Pro'     => '400,700',
        // 'Fira Sans'           => '400,700',
        // 'Ubuntu'              => '400,700',
        // 'Ubuntu Condensed'    => '400',
        // 'Oswald'              => '400,700',
        // 'Arimo'               => '400,700',
        // 'Exo 2'               => '400,700',
        // 'Yanone Kaffeesatz'   => '400,700',
        // 'PT Serif'            => '400,700',
        // 'Noto Serif'          => '400,700',
        // 'Noto Serif SC'       => '400,700',
        // 'Noto Serif TC'       => '400,700',
        // 'Merriweather'        => '400,700',
        // 'Roboto Slab'         => '400,700',
        // 'Lora'                => '400,700',
        // 'EB Garamond'         => '400,700',
        // 'Vollkorn'            => '400,700',
        // 'Tinos'               => '400,700',
        // 'Prata'               => '400',
        // 'Alice'               => '400',
        // 'Spectral'            => '400',
    ],

    /*
    |--------------------------------------------------------------------------
    | Local Fonts List
    |--------------------------------------------------------------------------
    |
    | To use local fonts instead of Google ones, add them to local_fonts list.
    |
    | Place your local fonts to the asset directory named `fonts`.
    | Name your local fonts in slugified way according to its weight:
    | - arial-regular.eot, arial-regular.woff, etc
    | - arial-italic.eot, arial-italic.woff, etc
    | - times-new-roman-bold.eot, times-new-roman-bold.woff, etc
    |
    | Allowed font formats: eot, woff, woff2, ttf
    |
    */

    'local_fonts' => [
        // 'Arial' => [
        //     '400'  => 'Arial Regular',
        //     '400i' => 'Arial Italic',
        //     '700'  => 'Arial Bold',
        //     '700i' => 'Arial Bold Italic',
        // ],
    ],

    /*
    |--------------------------------------------------------------------------
    | CSS filters
    |--------------------------------------------------------------------------
    |
    | Some filters against CSS files to be concatenated to a single CSS file.
    | Filters must implement Lamotivo/Assets/Filters/FilterContract
    | or extend Lamotivo/Assets/Filters/Filter.
    |
    */

    'css_filters' => [
        new Lamotivo\Assets\Filters\ScssCompiler,
        new Lamotivo\Assets\Filters\CssMin,
        new Lamotivo\Assets\Filters\CssRewriteUrls,
    ],

    /*
    |--------------------------------------------------------------------------
    | JS filters
    |--------------------------------------------------------------------------
    |
    | Some filters against JS files to be concatenated to a single JS file.
    | Filters must implement Lamotivo/Assets/Filters/FilterContract
    | or extend Lamotivo/Assets/Filters/Filter.
    |
    */

    'js_filters' => [
        new Lamotivo\Assets\Filters\JsMin,
        new Lamotivo\Assets\Filters\NewLine,
    ],

    /*
    |--------------------------------------------------------------------------
    | SCSS options
    |--------------------------------------------------------------------------
    |
    | With this group of options you can set some specific SCSS options.
    |
    */

    'scss' => [

        /*
         * SCSS formatter:
         * - expanded
         * - nested
         * - compact
         * - compressed
         * - crunched
         */

        'formatter' => env('ASSETS_SCSS_FORMATTER', 'compact'),

        /*
         * Import paths for SCSS compiler.
         * The list is always appended with the current SCSS file location.
         */

        'paths' => [
            // resource_path(),
        ],

        /*
         * Predefined SCSS variables.
         */

        'vars' => [
        ],

        /*
         * Predefined SCSS functions. Each function must be callable.
         */

        'functions' => [
            // 'theme-url' => new Lamotivo\Themes\ScssFunctions\ThemeUrl,
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Notifiers list
    |--------------------------------------------------------------------------
    |
    | Here you can set your own notifiers to process compiled assets.
    | For example, you may want upload a compiled asset file to you CDN.
    | Notifiers must implement Lamotivo/Assets/Notifiers/NotifierContract
    | or extend Lamotivo/Assets/Notifiers/Notifier.
    |
    */

    'notifiers' => [],

    /*
    |--------------------------------------------------------------------------
    | Asset collections
    |--------------------------------------------------------------------------
    |
    | Some predefined asset collections to use in your application.
    | External assets will be downloaded to temp_path and filtered.
    |
    */

    'collections' => [

        'jquery' => 'jquery2',

        'jquery1' => 'https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js',

        'jquery2' => 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js',

        'jquery3' => 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js',

        'bootstrap' => 'bootstrap3',

        'bootstrap3' => [
            'jquery',
            'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.0/js/bootstrap.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.0/css/bootstrap.min.css',
        ],

        'bootstrap4' => [
            'jquery',
            'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/css/bootstrap.min.css',
        ],

        'font-awesome' => 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css',

        'toastr' => 'https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js',

        'flowjs' => 'https://cdnjs.cloudflare.com/ajax/libs/flow.js/2.13.1/flow.min.js',

        'moment' => 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js',

        'modernizr' => 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js',

        'chartjs' => 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js',

        'trix' => 'https://cdnjs.cloudflare.com/ajax/libs/trix/1.0.0/trix.js',

        'jquery-ui' => [
            'jquery',
            'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js',
        ],

        'datatables' => [
            'jquery',
            'https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/jquery.dataTables.min.css',
        ],

        'bootstrap-daterangepicker' => [
            'bootstrap',
            'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.js',
        ],


        'angularjs' => 'https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.5/angular.min.js',

        'angular-resource' => [
            'angularjs',
            'https://cdnjs.cloudflare.com/ajax/libs/angular-resource/1.7.5/angular-resource.min.js',
        ],

        'angular-sanitize' => [
            'angularjs',
            'https://cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.7.5/angular-sanitize.min.js',
        ],

        'angular-touch' => [
            'angularjs',
            'https://cdnjs.cloudflare.com/ajax/libs/angular-touch/1.7.5/angular-touch.min.js',
        ],

        'angular-bootstrap' => [
            'angularjs',
            'https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/2.5.0/ui-bootstrap-tpls.min.js',
        ],

        'angular-ui-router' => [
            'angularjs',
            'https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/1.0.21/angular-ui-router.min.js',
        ],

        'angular-loading-bar' => [
            'angularjs',
            'https://cdnjs.cloudflare.com/ajax/libs/angular-loading-bar/0.9.0/loading-bar.min.js',
        ],

        'angular-ui-select' => [
            'angularjs',
            'https://cdnjs.cloudflare.com/ajax/libs/angular-ui-select/0.20.0/select.min.js',
        ],

        'angular-ui-grid' => [
            'angularjs',
            'https://cdnjs.cloudflare.com/ajax/libs/angular-ui-grid/4.6.6/ui-grid.min.js',
        ],

        'angular-chartjs' => [
            'angularjs',
            'chartjs',
            'https://cdnjs.cloudflare.com/ajax/libs/angular-chart.js/1.1.1/angular-chart.min.js',
        ],

        'angular-ui-tree' => [
            'angularjs',
            'jquery',
            'https://cdnjs.cloudflare.com/ajax/libs/angular-ui-tree/2.22.6/angular-ui-tree.min.js',
        ],

        'angular-ui-mask' => [
            'angularjs',
            'https://cdnjs.cloudflare.com/ajax/libs/angular-ui-mask/1.8.7/mask.min.js',
        ],

        'ngflow' => [
            'angularjs',
            'https://cdnjs.cloudflare.com/ajax/libs/ng-flow/2.7.8/ng-flow-standalone.min.js',
        ],

        'textangular' => [
            'angularjs',
            'https://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.16/textAngular.min.js',
        ],

        'vue' => 'https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.21/vue.min.js',

        'vue-router' => [
            'vue',
            'https://cdnjs.cloudflare.com/ajax/libs/vue-router/3.0.2/vue-router.min.js',
        ],

        'vue-resource' => [
            'vue',
            'https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.5.1/vue-resource.min.js',
        ],

    ],


];
