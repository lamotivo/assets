# Lamotivo Assets

## Introduction

A Laravel package to manage assets in your application.

## Installation

```
composer require lamotivo/assets
```

Then, publish the config

```
php artisan vendor:publish --provider="Lamotivo\Assets\AssetServiceProvider" --tag=config
```
